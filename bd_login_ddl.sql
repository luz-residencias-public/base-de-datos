SET FOREIGN_KEY_CHECKS=0;
DROP TABLE rol;
DROP TABLE usuario;
DROP TABLE permiso;
DROP TABLE rol_permiso;
DROP TABLE documento;
DROP TABLE documento_usuario;
SET FOREIGN_KEY_CHECKS=1;

create table rol
(
id INT PRIMARY KEY AUTO_INCREMENT,
nombre_rol VARCHAR(50) NOT NULL,
codigo_rol VARCHAR(50) NOT NULL,
activo BOOLEAN,
fh_creacion DATETIME,
fh_modificacion DATETIME,
usu_creacion INT,
usu_modificacion INT
);


create table usuario
(
id INT PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50) NOT NULL,
apellido_paterno VARCHAR(50) NOT NULL,
apellido_materno VARCHAR(50),
email VARCHAR(100) NOT NULL,
contrasenia VARCHAR(100) NOT NULL,
num_expediente VARCHAR(50) NOT NULL,
id_rol INT NOT NULL,
activo BOOLEAN,
fh_creacion DATETIME,
fh_modificacion DATETIME,
usu_creacion INT,
usu_modificacion INT,
CONSTRAINT fk_usu_id_rol FOREIGN KEY (id_rol) REFERENCES rol (id)
);

create table permiso
(
id INT PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(50) NOT NULL,
codigo_permiso VARCHAR(50) NOT NULL,
activo BOOLEAN,
fh_creacion DATETIME,
fh_modificacion DATETIME,
usu_creacion INT,
usu_modificacion INT
);

create table rol_permiso
(
id INT PRIMARY KEY AUTO_INCREMENT,
id_rol INT,
id_permiso INT,
activo BOOLEAN,
fh_creacion DATETIME,
fh_modificacion DATETIME,
usu_creacion INT,
usu_modificacion INT,
CONSTRAINT fk_rlp_id_rol FOREIGN KEY (id_rol) REFERENCES rol (id),
CONSTRAINT fk_rlp_id_permiso FOREIGN KEY (id_permiso) REFERENCES permiso (id)
); 

CREATE TABLE documento
 (
    id INT AUTO_INCREMENT PRIMARY KEY,
    tipo_documento VARCHAR(100) NOT NULL,
    tipo_archivo VARCHAR(50) NOT NULL,
    nombre_documento VARCHAR(255) NOT NULL,
    descripcion TEXT,
    ruta_documento VARCHAR(255) NOT NULL,
    documento_base64 LONGTEXT,
    documento_firmado_base64 LONGTEXT,
    contenido LONGTEXT,
    activo BOOLEAN,
    fh_creacion DATETIME,
    fh_modificacion DATETIME,
    usu_creacion INT,
    usu_modificacion INT
    
);

CREATE TABLE documento_usuario
(
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT, 
    id_documento INT,    
    activo BOOLEAN,
    fh_creacion DATETIME DEFAULT CURRENT_TIMESTAMP,
    fh_modificacion DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    usu_creacion INT,
    usu_modificacion INT,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id),
    FOREIGN KEY (id_documento) REFERENCES documento(id)
);