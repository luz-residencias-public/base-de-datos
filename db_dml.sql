INSERT INTO rol (nombre_rol, codigo_rol, activo, fh_creacion, fh_modificacion, usu_creacion, usu_modificacion)
VALUES
	('Superusuario', 'SUPER', TRUE, NOW(), NOW(), 1, 1), ('Administrador', 'ADMIN', TRUE, NOW(), NOW(), 1, 1),
    ('Usuario Básico', 'BASICO', TRUE, NOW(), NOW(), 1, 1),
    ('Operativo', 'OPER', TRUE, NOW(), NOW(), 1, 1);
    
INSERT INTO permiso (nombre, codigo_permiso, activo, fh_creacion, fh_modificacion, usu_creacion, usu_modificacion)
VALUES
    ('Dar de alta ausuarios', 'ALTA_USR', TRUE, NOW(), NOW(), 1, 1), ('Eliminar Usuario', 'ELIMINAR_USR', TRUE, NOW(), NOW(), 1, 1),
    ('Editar Usuario', 'EDITAR_USR', TRUE, NOW(), NOW(), 1, 1), ('Visualizar usuario', 'VISUALIZAR_USR', TRUE, NOW(), NOW(), 1, 1),
    ('Crear documentos', 'CREAR_DOCS', TRUE, NOW(), NOW(), 1, 1), ('Eliminar documento', 'ELIMINAR_DOC', TRUE, NOW(), NOW(), 1, 1),
    ('Editar documento', 'EDITAR_DOC', TRUE, NOW(), NOW(), 1, 1), ('Consultar documento', 'CONSULTAR_DOC', TRUE, NOW(), NOW(), 1, 1),
    ('Firmar documento', 'FIRMAR_DOC', TRUE, NOW(), NOW(), 1, 1),     ('Aprobar documentos', 'APROBAR_DOCS', TRUE, NOW(), NOW(), 1, 1);
	
INSERT INTO usuario (nombre, apellido_paterno, apellido_materno, email, contrasenia, num_expediente, id_rol, activo, fh_creacion, fh_modificacion, usu_creacion, usu_modificacion)
VALUES 
    ('Luz', 'Bautista', 'Diaz', 'luz@example.com', 'mypassword', 'EXP123', 1, TRUE, NOW(), NOW(), 1, 1), 
    ('Piedad', 'Hernandez', 'Flores', 'piedad@example.com', 'pie123', 'EXP125', 2, TRUE, NOW(), NOW(), 2, 2),
    ('Maria', 'Rodriguez', 'Cansino', 'maria@example.com', 'mar367', 'EXP163', 3, TRUE, NOW(), NOW(), 3, 3),
    ('Bertha', 'Gomez', 'Rosales', 'bety@example.com', 'bta890', 'EXP223', 3, TRUE, NOW(), NOW(), 3, 3),
    ('Javier', 'Santander', 'Sanchez', 'Javi@example.com', 'contrjavi', 'EXP134', 3, TRUE, NOW(), NOW(), 3, 3),
    ('Pedro', 'Diaz', 'Rodriguez', 'piter@example.com', 'piter963', 'EXP156', 4, TRUE, NOW(), NOW(), 4, 4);
	
INSERT INTO rol_permiso (id_rol, id_permiso, activo, fh_creacion, fh_modificacion, usu_creacion, usu_modificacion)
VALUES
(1, 1, TRUE, NOW(), NOW(), 1, 1), (1, 2, TRUE, NOW(), NOW(), 1, 1),
(1, 3, TRUE, NOW(), NOW(), 1, 1), (1, 4, TRUE, NOW(), NOW(), 1, 1),
(1, 5, TRUE, NOW(), NOW(), 1, 1), (1, 6, TRUE, NOW(), NOW(), 1, 1),
(1, 7, TRUE, NOW(), NOW(), 1, 1), (1, 8, TRUE, NOW(), NOW(), 1, 1),
(1, 9, TRUE, NOW(), NOW(), 1, 1), (1, 10, TRUE, NOW(), NOW(), 1, 1),
(2, 4, TRUE, NOW(), NOW(), 1, 1), (2, 5, TRUE, NOW(), NOW(), 1, 1),
(2, 6, TRUE, NOW(), NOW(), 1, 1), (2, 7, TRUE, NOW(), NOW(), 1, 1),
(2, 8, TRUE, NOW(), NOW(), 1, 1), (2, 9, TRUE, NOW(), NOW(), 1, 1),
(2, 10, TRUE, NOW(), NOW(), 1, 1), (3, 5, TRUE, NOW(), NOW(), 1, 1),
(3, 6, TRUE, NOW(), NOW(), 1, 1), (3, 7, TRUE, NOW(), NOW(), 1, 1),
(3, 8, TRUE, NOW(), NOW(), 1, 1), (3, 9, TRUE, NOW(), NOW(), 1, 1),
(3, 10, TRUE, NOW(), NOW(), 1, 1), (4, 5, TRUE, NOW(), NOW(), 1, 1),
(4, 6, TRUE, NOW(), NOW(), 1, 1), (4, 7, TRUE, NOW(), NOW(), 1, 1),
(4, 8, TRUE, NOW(), NOW(), 1, 1);